# 企业安全脑图

#### 项目介绍
企业安全脑图，结合互联网上的信息进行整合梳理，脑图中会注明来源，如有侵权，请联系我进行删除
本项目用来记录个人在工作中针对企业安全建设方面的一些脑图集合，感兴趣的可以看看，资料来源互联网。
图片不清晰的可以下载查看html版

#### 目录结构
![结构图](https://gitee.com/uploads/images/2018/0614/110428_a6f3d40b_1390378.jpeg "结构图.jpg")

### 参考信息
<br>https://github.com/SecWiki/sec-chart       //安全思维导图集合</br>
<br>http://www.secsky.cn/category/ent-sec                //安全天空</br>
<br>http://www.freebuf.com/articles/security-management/126202.html    //freebuf</br>
<br>https://tech.meituan.com/Data_Security_System_Construction.html    //美团技术团队</br>

